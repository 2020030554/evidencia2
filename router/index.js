const express = require("express");
const router = express.Router();
const bodyParse = require("body-parser");

//PExamen
router.get('/pexamen', (req, res) => {
    const num_boleto = req.body.num_boleto;
    const destino = req.body.destino;
    const nombre = req.body.nombre;
    const edad = req.body.edad;
    let tipo_viaje = req.body.tipo_viaje;
    const precio = req.body.precio;
    res.render('pexamen.html', num_boleto, destino, nombre, edad, tipo_viaje, precio);
  });
  
  router.post('/pexamen', (req, res) => {
    const num_boleto = req.body.num_boleto;
    const destino = req.body.destino;
    const nombre = req.body.nombre;
    const edad = req.body.edad;
    let tipo_viaje = req.body.tipo_viaje;
    const precio = req.body.precio;
  
    let subtotal = precio;

    if (tipo_viaje == 2) {
      subtotal = precio * 1.8;
    }
  
    let descuento = 0;
    if (edad >= 60) {
      descuento = subtotal * 0.5;
    }
  
    const impuesto = subtotal * 0.16;
    const total = (subtotal) - descuento + impuesto;
  
    res.render('pexamen.html', {
      num_boleto,
      destino,
      nombre,
      edad,
      tipo_viaje,
      precio,
      subtotal,
      impuesto,
      descuento,
      total,
    });
  });



module.exports=router;
